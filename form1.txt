//form1.cs
//Author: Barich, Alex
//Description: This is a program to create and display calendar events
//Class: CS3160 Section 1 Spring 2017

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab3
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //start off with the delete button disabled and
            //a pointer is set to the listBox Events
            btnDelete.Enabled = false;
            Form2.ListBox = listEvent;

        }

        //function name: btnNew_Click
        //description: click event for new button that creates second form event
        //parameters: object and event handler
        //return value: void
        private void btnNew_Click(object sender, EventArgs e)
        {
            //try catch
            try
            {
                //create a new form and set equal to form2
                Form2 form2 = new Form2();
                //display new form
                form2.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }//end catch

        }

        //function name: btnDelete_Click
        //description: deletes objects from list events
        //parameters: object and event handler
        //return value: void
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //check is listEvent is pointing anywhere
            if(listEvent.SelectedIndex != -1)
            {
                //create array of objects and set to where listEvent is pointing and remove both the object and the item
                Event[] obj = Form2.CreateEvent;
                obj[listEvent.SelectedIndex] = null;
                for(int i = listEvent.SelectedIndex; i < obj.Length && i < listEvent.Items.Count; i++)
                {
                    obj[i] = obj[i + 1];
                }
                listEvent.Items.RemoveAt(listEvent.SelectedIndex);
            }


            //clear all bxes
            txtTitle.Clear();
            txtDate.Clear();
            txtDay.Clear();
            txtTime.Clear();

            //Set the focus to the New button and disable the delete button if no items left in listBox
            btnNew.Focus();
            if(listEvent.Items.Count == 0)
            {
                btnDelete.Enabled = false;
            }
        }

        //function name: listEvent_SelectedIndexChanged
        //description: enables the delete button given there are items and displays all info in textboxes based on selected item
        //parameters: object and event handler
        //return value: void
        private void listEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Check to see if an item is selected, if it is, enable the delete button
            btnDelete.Enabled = true;
            if(listEvent.SelectedIndex != -1)
            {
                //create an array of events and set to event that list is pointing to
                //update all boxes with needed information
                Event[] obj = Form2.CreateEvent;
                txtTime.Text = obj[listEvent.SelectedIndex].getStart.TimeOfDay.ToString();
                txtTime.Text += obj[listEvent.SelectedIndex].getMS + " to " + obj[listEvent.SelectedIndex].getEnd.TimeOfDay.ToString() + obj[listEvent.SelectedIndex].getME;
                txtTitle.Text = obj[listEvent.SelectedIndex].Title.ToString();
                txtDay.Text = obj[listEvent.SelectedIndex].getStart.DayOfWeek.ToString();
                txtDate.Text = obj[listEvent.SelectedIndex].getStart.Date.ToString("dd/MM/yyyy");
            }

        }

    }//end form1

    public class Event
    {
        //start and end times as well as string for title and strings for AM/PM values
        private DateTime strtTime;
        private DateTime endTime;
        private string eventTitle;
        private string meridiemEnd;
        private string meridiemStrt;

        //function name: Event
        //description: constructor that takes 9 parameters to set both DateTime and strings
        //parameters: months day, starting hour and minute, ending hour and minute, title, start AM/PM, end AM/PM
        //return value: none
        public Event(int Month, int Day, int strtHour, int strtMinute, int endHour, int endMinute, string Title, string meridiemS, string meridiemE)
        {
            //check that the start hour and minutes do not conflict
            if(strtHour == endHour && strtMinute >= endMinute)
            {
                throw new ApplicationException("conflicting times");
            }
            else if(strtHour > endHour)
            {
                throw new ApplicationException("conflicting times");
            }

            //set start and endtimes with date objects
            strtTime = new DateTime(DateTime.Today.Year, Month, Day, strtHour, strtMinute, 0);
            endTime = new DateTime(DateTime.Today.Year, Month, Day, endHour, endMinute, 0);
            eventTitle = Title;
            meridiemEnd = meridiemE;
            meridiemStrt = meridiemS;

            //check that title has a value
            if (eventTitle == "" || eventTitle == null)
                throw new ApplicationException("Empty Title");
        }

        //function name: getStart
        //description: read only gets start time
        //parameters: none
        //return value: DateTime
        public DateTime getStart { get { return strtTime; } }

        //function name: getEnd
        //description: read only gets end time
        //parameters: none
        //return value: DateTime
        public DateTime getEnd { get { return endTime; } }

        //function name: Title
        //description: read only gets title
        //parameters: none
        //return value: string
        public string Title { get { return eventTitle; } }

        //function name: getMS
        //description: returns start time meridiem
        //parameters: none
        //return value: string
        public string getMS { get { return meridiemStrt; } }

        //function name: getME
        //description: returns end time meridiem
        //parameters: none
        //return value: string
        public string getME { get { return meridiemEnd; } }

        //function name: ToString
        //description: override to string formatted
        //parameters: none
        //return value: string
        public override string ToString()
        {
            return $"{strtTime.Year} -- {strtTime.Month} -- {strtTime.Day} {strtTime.Hour} : {strtTime.Minute} {meridiemStrt}";
        }

        //function name: Equals
        //description: checks if there are conflicting thimes that are equal to one other
        //parameters: Event object
        //return value: boolean
        public override bool Equals(object obj)
        {
            //check that obj contains a value
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;

            //Create a foo Events object
            Event foo;

            //Ensure that the object sent is both an object and is an event object
            if ((obj == null) || (!(obj is Event)))
                return false;

            //Place the object in the foo object
            foo = (Event)obj;

            //Compare the dates and times to see if they are equal / conflicting
            
            if (this.strtTime <= foo.strtTime && this.endTime >= foo.strtTime)
            {
                return true;
            }
            else if(this.strtTime <= foo.endTime && this.endTime >= foo.endTime)
            {
                return true;
            }

            return strtTime.Equals(obj);
        }

        //function name: GetHashCode
        //description: gets hashcode of object
        //parameters: none
        //return value: int
        public override int GetHashCode()
        {
            return strtTime.GetHashCode();
        }


    }//class Event
}
