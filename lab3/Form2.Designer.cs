﻿namespace lab3
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monthEvent = new System.Windows.Forms.MonthCalendar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboMeridiemSt = new System.Windows.Forms.ComboBox();
            this.comboMinuteSt = new System.Windows.Forms.ComboBox();
            this.comboHourSt = new System.Windows.Forms.ComboBox();
            this.comboMeridiemEnd = new System.Windows.Forms.ComboBox();
            this.comboMinuteEnd = new System.Windows.Forms.ComboBox();
            this.comboHourEnd = new System.Windows.Forms.ComboBox();
            this.textEventTitle = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // monthEvent
            // 
            this.monthEvent.Location = new System.Drawing.Point(18, 18);
            this.monthEvent.Name = "monthEvent";
            this.monthEvent.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(277, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "End Time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(277, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Start Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Event Description";
            // 
            // comboMeridiemSt
            // 
            this.comboMeridiemSt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMeridiemSt.FormattingEnabled = true;
            this.comboMeridiemSt.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboMeridiemSt.Location = new System.Drawing.Point(551, 75);
            this.comboMeridiemSt.Name = "comboMeridiemSt";
            this.comboMeridiemSt.Size = new System.Drawing.Size(60, 24);
            this.comboMeridiemSt.TabIndex = 4;
            // 
            // comboMinuteSt
            // 
            this.comboMinuteSt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMinuteSt.FormattingEnabled = true;
            this.comboMinuteSt.Items.AddRange(new object[] {
            "00",
            "15",
            "30",
            "45"});
            this.comboMinuteSt.Location = new System.Drawing.Point(412, 75);
            this.comboMinuteSt.Name = "comboMinuteSt";
            this.comboMinuteSt.Size = new System.Drawing.Size(104, 24);
            this.comboMinuteSt.TabIndex = 5;
            // 
            // comboHourSt
            // 
            this.comboHourSt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboHourSt.FormattingEnabled = true;
            this.comboHourSt.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboHourSt.Location = new System.Drawing.Point(280, 75);
            this.comboHourSt.Name = "comboHourSt";
            this.comboHourSt.Size = new System.Drawing.Size(98, 24);
            this.comboHourSt.TabIndex = 6;
            // 
            // comboMeridiemEnd
            // 
            this.comboMeridiemEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMeridiemEnd.FormattingEnabled = true;
            this.comboMeridiemEnd.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboMeridiemEnd.Location = new System.Drawing.Point(551, 148);
            this.comboMeridiemEnd.Name = "comboMeridiemEnd";
            this.comboMeridiemEnd.Size = new System.Drawing.Size(60, 24);
            this.comboMeridiemEnd.TabIndex = 7;
            // 
            // comboMinuteEnd
            // 
            this.comboMinuteEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMinuteEnd.FormattingEnabled = true;
            this.comboMinuteEnd.Items.AddRange(new object[] {
            "00",
            "15",
            "30",
            "45"});
            this.comboMinuteEnd.Location = new System.Drawing.Point(412, 148);
            this.comboMinuteEnd.Name = "comboMinuteEnd";
            this.comboMinuteEnd.Size = new System.Drawing.Size(104, 24);
            this.comboMinuteEnd.TabIndex = 8;
            // 
            // comboHourEnd
            // 
            this.comboHourEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboHourEnd.FormattingEnabled = true;
            this.comboHourEnd.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.comboHourEnd.Location = new System.Drawing.Point(280, 148);
            this.comboHourEnd.Name = "comboHourEnd";
            this.comboHourEnd.Size = new System.Drawing.Size(98, 24);
            this.comboHourEnd.TabIndex = 9;
            // 
            // textEventTitle
            // 
            this.textEventTitle.Location = new System.Drawing.Point(18, 237);
            this.textEventTitle.Name = "textEventTitle";
            this.textEventTitle.Size = new System.Drawing.Size(593, 22);
            this.textEventTitle.TabIndex = 10;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(18, 289);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(536, 289);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // Form2
            // 
            this.AcceptButton = this.btnAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(643, 409);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.textEventTitle);
            this.Controls.Add(this.comboHourEnd);
            this.Controls.Add(this.comboMinuteEnd);
            this.Controls.Add(this.comboMeridiemEnd);
            this.Controls.Add(this.comboHourSt);
            this.Controls.Add(this.comboMinuteSt);
            this.Controls.Add(this.comboMeridiemSt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.monthEvent);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form2";
            this.Text = "New Event";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar monthEvent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboMeridiemSt;
        private System.Windows.Forms.ComboBox comboMinuteSt;
        private System.Windows.Forms.ComboBox comboHourSt;
        private System.Windows.Forms.ComboBox comboMeridiemEnd;
        private System.Windows.Forms.ComboBox comboMinuteEnd;
        private System.Windows.Forms.ComboBox comboHourEnd;
        private System.Windows.Forms.TextBox textEventTitle;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAdd;
    }
}