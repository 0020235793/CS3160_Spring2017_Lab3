﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab3
{
    public partial class Form2 : Form
    {
        //pointer to listEvent in main form and Event array to hold objects
        private static ListBox list;
        private static Event [] createEvent = new Event[25];

        //function name: ListBox
        //description: getter and setter
        //parameters: non
        //return value: ListBox
        public static ListBox ListBox
        {
            get { return list; }

            set { list = value; }
        }

        //function name: CreateEvent
        //description: getter
        //parameters: none
        //return value: Event array
        public static Event[] CreateEvent
        {
            get
            {
                return createEvent;
            }
        }

        public Form2()
        {
            InitializeComponent();
        }

        //function name: btnAdd_Click
        //description: add event that takes all input and send to listEvent while checking for validation
        //parameters: object and event handler
        //return value: void
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //split the date from the calendar and put into respective integers
            string[] date = monthEvent.SelectionRange.Start.ToShortDateString().Split('/');
            int month, day, year;
            day = Convert.ToInt32(date[1]);
            year = Convert.ToInt32(date[2]);
            month = Convert.ToInt32(date[0]);
            //create dummy object
            Event add = null;

            //create validation markers
            bool convertS = false;
            bool convertE = false;
            int fixS = 0;
            int fixE = 0;

            //check that hours are over 12 AM if so add 12 hours and set respective boolean to true
            if(comboMeridiemEnd.Text == "PM" && comboMeridiemSt.Text == "PM")
            {
                convertE = true;
                fixE = 12 + Convert.ToInt32(comboHourEnd.Text);
                convertS = true;
                fixS = 12 + Convert.ToInt32(comboHourSt.Text);
            }
            else if (comboMeridiemSt.Text == "PM")
            {
                convertS = true;
                fixS = 12 + Convert.ToInt32(comboHourSt.Text);
            }
            else if(comboMeridiemEnd.Text == "PM")
            {
                convertE = true;
                fixE = 12 + Convert.ToInt32(comboHourEnd.Text);
            }

            //check for which case is needed based on which information to pass into new event and create the object
            if(convertE == true && convertS == true)
            {
                add = new Event(month, day, fixS, Convert.ToInt32(comboMinuteSt.Text), fixE, Convert.ToInt32(comboMinuteEnd.Text), textEventTitle.Text, comboMeridiemSt.Text, comboMeridiemEnd.Text);
            }
            else if (convertS == true)
            {
                add = new Event(month, day, fixS, Convert.ToInt32(comboMinuteSt.Text), Convert.ToInt32(comboHourEnd.Text), Convert.ToInt32(comboMinuteEnd.Text), textEventTitle.Text, comboMeridiemSt.Text, comboMeridiemEnd.Text);
            }
            else if(convertE == true)
            {
                add = new Event(month, day, Convert.ToInt32(comboHourSt.Text), Convert.ToInt32(comboMinuteSt.Text), fixE, Convert.ToInt32(comboMinuteEnd.Text), textEventTitle.Text, comboMeridiemSt.Text, comboMeridiemEnd.Text);
            }
            else
            {
                add = new Event(month, day, Convert.ToInt32(comboHourSt.Text), Convert.ToInt32(comboMinuteSt.Text), Convert.ToInt32(comboHourEnd.Text), Convert.ToInt32(comboMinuteEnd.Text), textEventTitle.Text, comboMeridiemSt.Text, comboMeridiemEnd.Text);
            }

            //go through list and check that no conflicting dates occur
            foreach (Event c in list.Items)
            {
                if (c.Equals(add))
                {
                    throw new ApplicationException("conflicting Hours");
                }
            }


            //add it to the list
            Event[] obj = CreateEvent;
            obj[list.Items.Count] = add;
            list.Items.Add(add);
           
            this.Close();

        }

        //function name: Form2_Load
        //description: set mac and min dates as well as initialize combo boxes
        //parameters:
        //return value:
        private void Form2_Load(object sender, EventArgs e)
        {
            //set mac date of calendar
            DateTime max = new DateTime(DateTime.Today.Year, 12, 31);

            //set minimum date to be selected
            monthEvent.MinDate = DateTime.Today;
            monthEvent.MaxDate = max;

            comboHourSt.SelectedIndex = 7;
            comboMinuteSt.SelectedIndex = 0;
            comboMeridiemSt.SelectedIndex = 0;

            comboHourEnd.SelectedIndex = 3;
            comboMinuteEnd.SelectedIndex = 0;
            comboMeridiemEnd.SelectedIndex = 1;
        }

        //function name: btnCancel_Click
        //description: exits out of new form
        //parameters: object and event handler
        //return value: void
        private void btnCancel_Click(object sender, EventArgs e)
        {
            //make sure object holds no information and close out
            Event[] obj = CreateEvent;
            obj[list.Items.Count] = null;
            this.Close();
        }
    }
}
